#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char word[50];
    char hash[33];
};

int file_length(char *filename)
{
    struct stat info;
    int ret = stat(filename, &info);
    if (ret == -1) return -1;
    else return info.st_size;
}
int hashcompare(const void *a, const void *b)
{
    return strcmp((char *)a, (*(struct entry *)b).hash);
}

int hashsearch(const void *target, const void *elem)
{
    return strcmp((char *)target, (*(struct entry *)elem).hash);
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    int len = file_length(filename);
    
    char *entry = malloc(len);
    FILE *f = fopen(filename, "r") ;
    if(!f)
    {
        printf("Cant open file: %s\n", filename);
        exit(1);
    }
    fread(entry, sizeof(char), len, f);
    fclose(f);
    
    int num = 0;
    for(int i = 0; i<len; i++)
    {
        if(entry[i] =='\n')
        {
            entry[i] = '\0';
            num++;
        }
    }
    struct entry *dict = malloc(num * sizeof(struct entry));
    
    sscanf(&entry[0], "%s %s", dict[0].word, dict[0].hash);
    char *hash = md5( dict[0].word, strlen(dict[0].word));
    strcpy(dict[0].hash, hash);
    
    int j=1;
    for (int i = 0; i < len-1; i++)
    {
        if (entry[i] == '\0')
        {
            sscanf(&entry[i+1], "%s %s", dict[j].word, dict[j].hash);
            char *hash = md5(dict[j].word, strlen(dict[j].word));
            strcpy(dict[j].hash, hash);
            j++;
        }
    }
     
    // for (int i = 0; i < 50 ; i++)
    // {
    //     printf("%d: %s  HASH:%s\n", i, dict[i].word, dict[i].hash);
    // }
    
    *size = len;
    free(dict);
    return dict;
}
int main(int argc, char *argv[])
 {
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1); 
    }
    int dlen;
    // TODO: Read the dictionary file into an array of entry structures
    struct entry *dict = read_dictionary( argv[2] , &dlen);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dlen, sizeof(struct entry), hashcompare);
    
    // TODO
    // Open the hash file for reading.
    char hashes[33];
    FILE *h = fopen("hashes.txt", "r");
    if(!h)
    {
        printf("Can not open\n");
    }
    fread(hashes, sizeof(char), HASH_LEN, h);
    fclose(h);
    
//     for(int i = 0; i< 10; i++)
//  {
//      printf(" hashes: %s\n", hashes);
//  }

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)


    struct entry *found = bsearch(hashes, dict, dlen, sizeof(struct entry), hashsearch);
    if (found == NULL)
    {
        printf("Not found\n");
    }
    else
    {
        printf("Found %s %s\n", found->word, found->hash);
    }
}
